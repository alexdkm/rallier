from django.shortcuts import render
from .models import user
from django.utils import timezone
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from datetime import timedelta, datetime
import threading
# Create your views here.
def home(request):
    data=user.objects.all()
    return render(request,'home.html',{'data':data})

def register(request):
    return render(request,'register.html')

def reg(request):
    ic = request.POST.get('ic')
    
    site = request.POST.get('site')
    passNum = request.POST.get('passnum')
    name = request.POST.get('name')

    qs = user.objects.filter(ic=ic)
    if qs.exists():
        messages.info(request, 'IC has registered')    
    else:
        num_obj = user(name = name, passNum = passNum, site = site, ic = ic)
        num_obj.save()
        messages.info(request, 'User registered, please activate the user.')    
        
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/register'))

def status(request):

    site = request.POST.get('site')
    data = user.objects.filter(site = site)
    #if 'checkIn' in request.POST:
    #    ic = request.POST.get('checkInIC')
    #    user.objects.filter(ic = ic).update(alert = True)
    #   return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    if 'checkIn' in request.POST:
        ic = request.POST.get('tag')
        abc=user.objects.filter(ic = ic).values('name')
        user.objects.filter(ic__in = ic).update(FirAlert = False, SecAlert = False)
        print(abc)
        messages.info(request, 'Saved!')
        
    elif 'alarmbtn' in request.POST:
        ic = request.POST.getlist('tag')
        alarm = request.POST.get('alarm')
        alarm = int('0' + alarm)
        interval = timedelta(minutes=alarm)
        clockIn = timezone.now() + interval
        user.objects.filter(ic__in = ic).update(alarm = alarm,FirTime = clockIn)
        abc=user.objects.filter(ic__in = ic).values('name')
        print(abc)
        messages.info(request, 'Saved!')

    return render(request,'status.html',{'data':data})

def abc(f_stop):
    hour = timezone.now().hour
    minute = timezone.now().minute
    second = timezone.now().second

    stop = user.objects.filter(FirTime__hour__lte=hour,FirTime__minute__lte=minute,FirTime__second__lte=second)
    secfail = user.objects.filter(SecTime__hour__lte=hour,SecTime__minute__lte=minute,SecTime__second__lte=second)
    for fail in secfail:
        check = user.objects.get(ic = fail).FirAlert
        if check == True:
            user.objects.filter(ic = fail).update(SecAlert=True)
    for value in stop:
        check = user.objects.get(ic = value).activate
        if check == True:
            print(value)
            interval = user.objects.get(ic = value).alarm
            grace = user.objects.get(ic = value).grace
            alarm = int(0 + interval)
            grace = int(0 + grace)
            interval = timedelta(minutes=alarm)
            grace = timedelta(minutes=grace)
            clockIn = timezone.now() + interval
            sectime = timezone.now() + grace
            user.objects.filter(ic = value).update(FirAlert=True,FirTime = clockIn,SecTime = sectime)

    if not f_stop.is_set():
        # call f() again in 60 seconds
        threading.Timer(1, abc, [f_stop]).start()

f_stop = threading.Event()
# start calling f now and every 60 sec thereafter
abc(f_stop)