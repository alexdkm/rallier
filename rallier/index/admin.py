from django.contrib import admin
from .models import user

# Register your models here.
class AllUser(admin.ModelAdmin):
    class Meta:
        model = user
        
admin.site.register(user, AllUser)