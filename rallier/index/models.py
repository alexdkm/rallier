from django.db import models
from django.db import models
from django.contrib import admin
from django.utils.timezone import now
from django.contrib.auth.models import User
from django.utils import timezone

from datetime import datetime, timedelta

import math
# Create your models here.
class user(models.Model):
    site = models.CharField(max_length=50,null=True)
    ic = models.CharField(max_length=20, default='',null=True,blank=True)
    name = models.CharField(max_length=50, default='',null=True,blank=True)
    passNum = models.CharField(max_length=20, default='',null=True,blank=True)
    FirAlert = models.BooleanField(default=False)
    SecAlert = models.BooleanField(default=False,null=True)
    FirTime = models.DateTimeField(auto_now=False,null=True,blank=True)
    SecTime = models.DateTimeField(auto_now=False,null=True)
    activate = models.BooleanField(default=True)
    alarm = models.IntegerField(default=900,null=True)
    grace = models.IntegerField(default=2,null=True)

    def __str__(self):
        return self.ic